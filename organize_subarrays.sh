#!/bin/bash

# Organizes subarray xh files from subarray*.txt files

if [ ! -f subarray_0.txt ]; then echo "Must have at least one subarray_?.txt file"; exit; fi

j=0
for i in subarray_*.txt; do
    mkdir SUBARRAY_${j}
    mv $i SUBARRAY_${j}
    cd SUBARRAY_${j}
    while read lat lon netw stat; do
       echo $netw $stat
       cat ../z_comp/*${netw}*${stat}*xh >> z.xh
       cat ../n_comp/*${netw}*${stat}*xh >> n.xh
       cat ../e_comp/*${netw}*${stat}*xh >> e.xh
    done < $i
    cd ..
    
    j=$((j+1))
done
