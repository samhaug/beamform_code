from sys import argv,exit
if len(argv) != 3:
   print("Usage: python subdivide_array info_file n_clusers")
   print("   Where info_file iis 4 column ascii file of format stla stlo stnm netwk")
   print("   n_clusters: number of subarrays")
   exit()

from sklearn.cluster import KMeans
import numpy as np
import obspy
from obspy.geodetics import gps2dist_azimuth


coords = np.genfromtxt(argv[1])[:,0:2]
names = open(argv[1],'r').readlines()
kmeans = KMeans(n_clusters=int(argv[2]))
kmeans.fit(coords)
centers = kmeans.cluster_centers_


for idx,ii in enumerate(centers):
   f = open("subarray_{}.txt".format(str(idx)),"w")
   for jdx,jj in enumerate(coords):
      dist = gps2dist_azimuth(ii[0],ii[1],jj[0],jj[1])[0]/111195.
      if dist < 5.:
         f.write("%8.4f %8.4f %4s %4s\n"%(jj[0],jj[1],names[jdx].split()[2],names[jdx].split()[3]))
          
   f.close()

