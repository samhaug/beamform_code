#!/bin/bash

alatlon=$(awk 'BEGIN{i=0;lat=0;lon=0} {lat+=$1;lon+=$2;i++} END{print lat/i,lon/i}' subarray*.txt)
elatlon=$(xh_shorthead z.xh | awk '{print $6,$7}' | head -n1)

baz=$(vincenty_inverse $alatlon $elatlon | awk '{print $1}')
baz=${baz%.*}

baz_min=$(($baz-60))
baz_max=$(($baz+60))
beam_name=$(echo subarray_*.txt)
beam_name=${beam_name::-4}.beam

echo "#!/bin/bash" > beam.sbatch
echo "#SBATCH --job-name=beamform" >> beam.sbatch
echo "#SBATCH --account=jritsema1" >> beam.sbatch
echo "#SBATCH --export=ALL " >> beam.sbatch
echo "#SBATCH --nodes=1" >> beam.sbatch
echo "#SBATCH --ntasks-per-node=1 " >> beam.sbatch
echo "#SBATCH --cpus-per-task=1" >> beam.sbatch
echo "#SBATCH --mem=175000m" >> beam.sbatch
echo "#SBATCH --time=01:00:00 " >> beam.sbatch
echo "#SBATCH --output=beam.out" >> beam.sbatch
echo "#SBATCH --error=beam.err " >> beam.sbatch
echo "xh_3compbeamform zw.xh nw.xh ew.xh $baz_min $baz_max 1 0 40 1 $beam_name" >> beam.sbatch
echo "xh_beamdescribe $beam_name > describe" >> beam.sbatch

sbatch beam.sbatch
